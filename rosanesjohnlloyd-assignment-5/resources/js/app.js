 /**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const app = new Vue({
 	el: '#app',
 	data: {
 		cname: '',
 		contactnumber: '',
 		users: ''
 	},
 	mounted: function mounted(){
 		this.getItems();
 	},
 	methods:{
 		getItems: function getItems(){
 			var _this = this;
 			axios.get('/getItems')
 			.then(function (response) {
 				_this.users = response.data;
 			})
 		},
 		addRecord: function addRecord(){
 			var name = this.cname;
 			var _this = this;
 			var contact = this.contactnumber;
 			if(name == '' || contact == ''){
 				alert("Error! All fields are required!");
 				
 			}
 			else{
 				axios.post('/storeItem', {
 					cname: this.cname,
 					contactnumber: this.contactnumber
 				})
 				.then(function(response){
 					alert(name + " was Successfully Added!");
 					//window.location.reload();
 					_this.getItems();
 				})
 			}
 		},
 		deleteContact: function deleteContact(contact){
 			var _this = this;
 			axios.post('/deleteContact/' + contact.id).then(function (response) {
 				_this.getItems();
 			})
 		}
 	}
 });
