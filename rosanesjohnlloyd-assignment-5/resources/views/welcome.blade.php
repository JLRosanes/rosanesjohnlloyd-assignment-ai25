<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Laravel and VueJS</title>

</head>
<body>
  <nav class="navbar navbar-dark bg-dark">
      <a class="navbar-brand lead" href="">Laravel and Vue</a>
  </nav>
  <div id="app">
    <div class="row mt-4 justify-content-center">
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Name</label>
            <div class="col-sm-8">
             <input type="text" class="form-control" id="name" name="name" 
             required="required" v-model="cname" placeholder="Name">
            </div>
        </div>
     <div class="form-group row">
        <label class="col-sm-4 col-form-label">Contact No.</label>
        <div class="col-sm-7">
            <input id="contactnumber" name="contactnumber" type="text" class="form-control" required v-model="contactnumber" placeholder="Contact Number">
        </div>
    </div>
    <div class="form-group row">
        <button class="btn btn-success btn-sm" @click.prevent="addRecord">Add</button>
    </div>
    <div class="container">
    <div class="d-flex justify-content-center">
      <table class="table table-hover" id="table">
        <thead class="thead-dark">
            <th>Name</th>
            <th>Contact Number</th>
            <th>Action</th>
        </thead>
        <tbody>
            <tr v-for="user in users">
                <td>@{{user.name}}</td>
                <td>@{{user.contactnumber}}</td>
                <td><button @click.prevent="deleteContact(user)" class="btn btn-danger btn-sm">Remove</button></td>
            </tr>
        </tbody>
      </table>
    </div>
    </div>
</div>
</div>
<script type="text/javascript" src="/js/app.js"></script>  
</body>
</html>
