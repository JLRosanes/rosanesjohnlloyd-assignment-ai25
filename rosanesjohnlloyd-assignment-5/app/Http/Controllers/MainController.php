<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;

class MainController extends Controller
{
    public function storeItem(Request $req){
    	$data = new Data();
    	$data->name = $req->cname;
    	$data->contactnumber = $req->contactnumber;
    	$data->save();

    	return $data;
    }

    public function getItems(Request $req){
    	$data = Data::all();
    	return $data;
    }

    public function deleteContact(Request $req){
        $data = Data::find($req->id)->delete();
    }
}
