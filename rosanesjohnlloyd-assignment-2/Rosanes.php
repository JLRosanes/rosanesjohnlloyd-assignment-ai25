<?php  
  $age = 18;
  
  echo "<br>If-Elseif-Else<br>";
  if($age > 18) {
    echo "You are adult!<br>";
  }elseif($age == 18) {
    echo "You are 18 years old!<br>";
  }else {
    echo "You are minor!<br>";
  }

  echo "<br>While Loop<br>";
  $w = 0;
  while($w < 5) {
    echo "This is While Loop!<br>";
    $w++;
  }

  echo "<br>Do-While Loop<br>";
  $w = 0;
  do {
    echo "This is Do-While Loop!<br>";
    $w++;
  }while($w < 5);

  echo "<br>For Loop<br>";
  for($w = 0; $w < 5; $w++) {
    echo "This is for Loop!<br>";
  }

  echo "<br>ForEach Loop<br>";
  $num = array(10, 20, 30, 50, 40);
  foreach($num as $n) {
    echo $n . "<br>";
  }
?>