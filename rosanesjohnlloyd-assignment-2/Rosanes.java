class Main {
  public static void main(String[] args) {
    int age = 18;
    if(age > 18) {
      System.out.println("You are adult.");
    }else if(age == 21) {
      System.out.println("You are 18 years old.");
    }else {
      System.out.println("You are minor");
    }

    System.out.println();

    System.out.println("While Loop");
    int w = 0;
    while(w < 5) {
      System.out.println("This is While Loop #"+w);
      w++;
    }

    System.out.println("Do-While Loop");
    w = 0;
    do {
      System.out.println("This is Do-While Loop #"+w);
      w++;
    }while(w < 5);

    System.out.println("For Loop");
    for(w = 0; w < 5; w++) {
      System.out.println("This is For While Loop"+w);
    }
    
    System.out.println("For-Each Loop");
    int num[] = {23, 345, 512, 523, 23};
    for(int n : num) {
      System.out.println(n);
    }
  }
}