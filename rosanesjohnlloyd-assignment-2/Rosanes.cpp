#include <stdio.h>

int main(void) {

  int age = 18;

  if(age > 18) {
    printf("You are adult!");
  }else if(age == 21) {
    printf("You are 21 years old!");
  }else {
    printf("You are minor!");
  }

  printf("\nWhile Loop\n");

  int w = 0;

  while(w < 5) {
    printf("This is While Loop\n");
    w++;
  }

  w = 0;
  printf("\nDo-While Loop\n");

  do {
    printf("This is Do-Loop!\n");
    w++;
  }while(w < 5);

  printf("\nFor Loop\n");

  for(w = 0; w < 5; w++) {
    printf("This is For While Loop\n");
  }

  return 0;
}