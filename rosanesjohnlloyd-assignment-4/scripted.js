var app = new Vue({
  el: '#phoneapp',
  data: {
    users: "",
    userid: 0,
    
    name: "",
    phone: ""
  },
  methods: {
   allRecords: function(){
     axios.post('ajaxfile.php', {
       request: 1
     })
     .then(function (response) {
       app.users = response.data;
     })
     .catch(function (error) {
       console.log(error);
     });
 
   },
   addRecord: function(){

     if(this.name != '' && this.phone != ''){
       axios.post('ajaxfile.php', {
         request: 2,
         
         name: this.name,
         phone: this.phone
       })
       .then(function (response) {

         
         app.allRecords();

         
         
         app.name = '';
         app.phone = '';
 
         alert(response.data);
       })
       .catch(function (error) {
         console.log(error);
       });
     }else{
       alert('Fill all fields.');
     }
 
   },
   updateRecord: function(index,id){

     // Read value from Textbox
     var name = this.users[index].name;
     var phone = this.users[index].phone;

     if(name !='' && phone !=''){
       axios.post('ajaxfile.php', {
         request: 3,
         id: id,
         name: name,
         phone: phone
       })
       .then(function (response) {
         alert(response.data);
       })
       .catch(function (error) {
         console.log(error);
       });
     }
   },
   deleteRecord: function(index,id){
 
     axios.post('ajaxfile.php', {
       request: 4,
       id: id
     })
     .then(function (response) {

       // Remove index from users
       app.users.splice(index, 1);
       alert(response.data);
     })
     .catch(function (error) {
       console.log(error);
     });
 
    } 
  },
  created: function(){
    this.allRecords();
  }
})
